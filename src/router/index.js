import Vue from "vue";
import Router from "vue-router";
import Home from "../views/Home.vue";
import Checks from "../components/User/ListChecks.vue";
import Add from "../components/Admin/AddCheck.vue";


Vue.use(Router);

let router = new Router({
  // mode: "history",
  routes: [
    {
      path: "/",
      nome: "/",
      component: Home,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/about",
      nome: "about",
      component: () => import("../views/About.vue"),
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: "/cadastro",
      nome: "cadastro",
      component: () => import("../views/cadastroUser.vue"),
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: "/login",
      nome: "login",
      component: () => import("../views/Login.vue"),
      meta: {
        requiresAuth: false,
      },
    },
    {
      path: "/checks",
      nome: "checks",
      component: Checks,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: "/add",
      nome: "add",
      component: Add,
      meta: {
        requiresAuth: true,
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    if (sessionStorage.getItem("jwt") == null) {
      next({
        path: "/login",
        params: { nextUrl: to.fullPath },
      });
    } else {
      next();
      //Verifica o nivel do usuario
      // let user = JSON.parse(localStorage.getItem('user'))
      // if(to.matched.some(record => record.meta.is_admin)) {
      //     if(user.is_admin == 1){
      //         next()
      //     }
      //     else{
      //         next({ name: '/'})
      //     }
      // }else {
      //     next()
      // }
    }
  } else if (to.matched.some((record) => record.meta.guest)) {
    if (sessionStorage.getItem("username") == null) {
      next();
    } else {
      next({ name: "/" });
    }
  } else {
    next();
  }
});

export default router;
